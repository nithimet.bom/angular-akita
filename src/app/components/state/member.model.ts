import { guid } from '@datorama/akita';

export interface Member {
    id: string;
    username: string;
    phone: string;
    completed: boolean;
}

export function createMember(username: string, phone: string) {
    return {
        id: guid(),
        username,
        phone,
        completed: false
    } as Member;
}
